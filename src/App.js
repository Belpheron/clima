import React, { Fragment, useEffect, useState } from "react";

import Clima from "./components/Clima";
import Formulario from "./components/Formulario";
import Header from "./components/Header";

function App() {
  const [busqueda, guardarBusqueda] = useState({
    ciudad: "",
    pais: "",
  });

  const [consultar, guardarConsultar] = useState(false);
  const [resultado, guardarResultado] = useState({});

  const { ciudad, pais } = busqueda;

  useEffect(() => {
    const consultarAPI = async () => {
      if (consultar) {
        const appid = "da12e18dcd051411ac6977da755b6fea";
        const url = `http://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appid}`;
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        guardarResultado(resultado);
      }
    };
    consultarAPI();
  }, [consultar, ciudad, pais]);

  return (
    <Fragment>
      <Header titulo="Clima React App" />

      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formulario
                busqueda={busqueda}
                guardarBusqueda={guardarBusqueda}
                guardarConsultar={guardarConsultar}
              />
            </div>
            <div className="col m6 s12">{<Clima resultado={resultado} />}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
